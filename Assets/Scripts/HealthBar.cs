﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour {

    public Image currentHealthbar;
    public GameObject Player;
    public float maxEnergy = 100;
    GhostController controller;
    


    // Use this for initialization
    void Start()
    {
        controller = Player.GetComponent<GhostController>();
        controller.getEnergy();
    }

	// Update is called once per frame
	void Update () {
            float ratio = controller.getEnergy() / maxEnergy;
            currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
	
	}
}
