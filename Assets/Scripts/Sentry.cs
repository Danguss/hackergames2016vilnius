﻿using UnityEngine;
using System.Collections;

public class Sentry : MonoBehaviour {
	public GameObject player;

    bool isAlert = false;

    AudioSource Audio;
	GhostController controller;


    void Start() { 
        Audio = GetComponent<AudioSource>();
		controller = player.GetComponent<GhostController>();
    }


    void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject == player) {
			isAlert = true;
			Audio.Play ();
		}
    }

    void OnTriggerStay(Collider collision)
    {
		if(isAlert)
        {
			controller.updateEnergy(-Time.deltaTime * 3.50f);
        }
    }

	void OnTriggerExit(Collider collision)
	{
		if (collision.gameObject == player) {
			isAlert = false;
			Audio.Stop ();
		}
	}
}
 
   


