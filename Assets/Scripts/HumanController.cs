﻿using UnityEngine;
using System.Collections;

public class HumanController: MonoBehaviour {
	public GameObject player;
	GhostController controller;
	bool scared = false;
	public bool awake;
	public GameObject newspaper;
	bool waitAsleep;
	Animator anim;
	// Use this for initialization
	void Start () {
		controller = player.GetComponent<GhostController>();
		anim = this.GetComponentInChildren<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		//NavMeshAgent agent = GetComponent<NavMeshAgent>();
		if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Nebeklauso muzikos")) {
			waitAsleep = true;
		}
		//agent.destination = player.transform.position;//this.transform.position * 2 - goal.transform.position;
		if (waitAsleep && anim.GetCurrentAnimatorStateInfo(0).IsName("Klauso muzikos")) {
			Debug.Log ("Good night");
			waitAsleep = false;
			awake = false;
		}
	}

	void OnTriggerEnter(Collider collision)
	{
		if (scared)
			return;
		if (collision.gameObject == player) {
			controller.nearHuman = this;
		}
	}

	void OnTriggerStay(Collider collision)
	{

	}

	void OnTriggerExit(Collider collision)
	{
		if (collision.gameObject == player) {
			controller.nearHuman = null;
		}
	}

	public bool scare(){
		Debug.Log (awake);
		if (!awake)
			return false;
		scared = true;
		this.GetComponentInChildren<ParticleSystem> ().Play ();
		if (this.GetComponent<Animator> ())
			this.GetComponent<Animator> ().Play ("Scared");
		if (newspaper) newspaper.SetActive (true);
		return true;
		//this.transform.localScale = new Vector3 (1f, 1f, 1f);
	}

	public void wake(){
		awake = true;

		Debug.Log ("Good morning");
		anim.Play ("Nebeklauso muzikos");
	}
}
