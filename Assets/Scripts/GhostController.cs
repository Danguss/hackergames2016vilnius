﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GhostController : MonoBehaviour {
	public float startingEnergy;
	public float minHowlEnergy;
    float energy;
	Rigidbody body;
	Animator anim;
	string state;
	public Text diedText;
	public FlowerController nearFlower;
	public HumanController nearHuman;
	public GameObject nearPossessable;
	bool moveTouch, possessed;
	public GameObject moveButton;
	public GameObject moveIndicator;
	public GameObject howlButton;
	public Text possessButtonText;
	AudioSource howlSound;
	AudioSource suckSound;
	float risingTimeLeft;
	public float risingTime;
	public float sinkingTime;
	Vector3 riseVector;
	bool won;
	public GameObject interPlane;
	public GameObject moved;
	bool movedGravity;

	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		GameData.currentScene = SceneManager.GetActiveScene ().buildIndex;
		GameData.highestSceneReached = Mathf.Max (GameData.highestSceneReached, GameData.currentScene);
		body = this.gameObject.GetComponent<Rigidbody> ();
		anim = this.gameObject.GetComponent<Animator> ();
		energy = startingEnergy;
		howlSound = GetComponents<AudioSource>()[0];
		suckSound = GetComponents<AudioSource>()[1];

		risingTimeLeft = risingTime;
		riseVector = new Vector3 (0f, 4f, 0f);
		this.transform.position = this.transform.position - riseVector;
		this.GetComponent<BoxCollider> ().enabled = false;
	}

	void die() {
		SceneManager.LoadScene ("Dead");
	}

	public void win(){
		Debug.Log ("Won!");
		riseVector = -riseVector;
		risingTimeLeft = sinkingTime;
		body.velocity = new Vector3 (0f, 0f, 0f);
		anim.Play ("Front");
		this.GetComponent<BoxCollider> ().enabled = false;
		won = true;
	}

	public void updateEnergy(float increase){
		energy += increase;
		if (energy < 0)
			die();
		howlButton.GetComponent<Button> ().interactable = (energy >= minHowlEnergy);
	}

	public float getEnergy() {return energy;}

	public void howl() {
		Debug.Log ("Boo!");
		if (nearHuman) {
			if (nearHuman.scare ())
				win ();
		}
		howlSound.Play ();
	}

	public void suck() {
		if (nearFlower) {
			updateEnergy (nearFlower.flowerPower);
			nearFlower.die ();
            suckSound.Play ();
		}
	}

	public void possess(){
		possessed = !possessed;
		possessButtonText.text = possessed ? "Release" : "Possess";
		if (possessed)
			body.velocity = new Vector3 (0, 0, 0);
		else
			nearPossessable.GetComponent<Rigidbody>().velocity = new Vector3 (0, 0, 0);
	}

	public void resetLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	void checkMoveables() {
		bool touched = (Input.touchCount > 0);
		bool moused = Input.GetMouseButton (0) || Input.GetMouseButtonDown (0) || Input.GetMouseButtonUp (0);
		if (touched || moused) {
			Ray ray = touched ? Camera.main.ScreenPointToRay(Input.touches [0].position) : Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (touched ? Input.touches [0].phase == TouchPhase.Began : Input.GetMouseButtonDown(0)) {
				//Debug.Log (Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100) && hit.collider.gameObject.tag == "Moveable") {
					//Debug.Log (hit.collider.gameObject.name);
					moved = hit.collider.gameObject;
					Rigidbody movedBody = moved.GetComponent<Rigidbody> ();
					if (movedBody) {
						movedGravity = movedBody.useGravity;
						movedBody.useGravity = false;
					} else
						movedGravity = false;
					interPlane.transform.position = moved.transform.position;
				}
			}
			if (moved) {
				if (Physics.Raycast (ray, out hit, 100, 1 << 8)) {
					moved.transform.position = hit.point;
				}
				if (touched ? Input.touches [0].phase == TouchPhase.Ended : Input.GetMouseButtonUp (0)) {
					if (movedGravity)
						moved.GetComponent<Rigidbody> ().useGravity = true;
					moved = null;
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (risingTimeLeft > 0) {
			risingTimeLeft -= Time.deltaTime;
			this.transform.position = this.transform.position + riseVector * Time.deltaTime / risingTime;
			if (risingTimeLeft <= 0) {
				if (won) {
					Debug.Log ("Won");
					SceneManager.LoadScene (GameData.currentScene + 1);
				} else {
					this.GetComponent<BoxCollider> ().enabled = true;
				}
			}
			return;
		}
		updateEnergy (-Time.deltaTime);
		float dx = 0f, dz = 0f;
		if (Input.touchCount > 0) {
			Touch touch = Input.touches [0];
			Vector2 origin = moveButton.transform.position;
			if (touch.phase == TouchPhase.Began && (touch.position - origin).magnitude < 60) {
				moveTouch = true;
				moveIndicator.SetActive (true);
			}
			if (moveTouch) {
				moveIndicator.transform.position = touch.position;
				Vector2 dpos = (touch.position - origin);
				if (dpos.magnitude > 10) {
					dpos = dpos / dpos.magnitude;
					dx = dpos.x * 4f;
					dz = dpos.y * 4f;
				}
			}
			if (touch.phase == TouchPhase.Ended) {
				moveTouch = false;
				moveIndicator.SetActive (false);
			}
		} else {
			dx = Input.GetAxis("Horizontal") * 4.0f;
			dz = Input.GetAxis("Vertical") * 4.0f;
		}

		if (possessed) {
			nearPossessable.GetComponent<Rigidbody>().velocity = new Vector3 (dx, 0, dz);
			dx = dz = 0;
		} else {
			body.velocity = new Vector3 (dx, 0, dz);
		}
		if (dx == 0 && dz == 0)
			anim.Play ("Front");
		else if (Mathf.Abs (dx) > Mathf.Abs (dz)) {
			if (dx > 0)
				anim.Play ("Right");
			else
				anim.Play ("Left");
		} else {
			if (dz > 0) anim.Play ("Back");
			else anim.Play ("Front");
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			howl ();
			suck ();
		}

		checkMoveables ();
	}
}
