﻿using UnityEngine;
using System.Collections;

public class FlowerController : MonoBehaviour {
	public GameObject player;
	public int flowerPower;
	GhostController controller;
	Animator anim;
	bool alive;

	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		controller = player.GetComponent<GhostController>();
		anim = this.gameObject.transform.GetChild(0).gameObject.GetComponent<Animator> ();
		alive = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void die() {
        GetComponentInChildren<ParticleSystem>().Play();
        alive = false;
		controller.nearFlower = null;
        anim.Play ("Flower dying");

    }

	public int getFlowerPower() {
		return flowerPower;
	}

	void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject == player && alive) {
			controller.nearFlower = this;
		}
	}
		

	void OnTriggerExit(Collider collision)
	{
		if (collision.gameObject == player) {
			controller.nearFlower = null;
		}
	}
}
