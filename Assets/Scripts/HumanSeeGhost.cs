﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HumanSeeGhost : MonoBehaviour {

    bool GhostIsSeen = false;
    public float DeathTimer = 3;
    public GameObject objekt;


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Ghost")
        {
            GhostIsSeen = true;
            objekt.transform.position = new Vector3 (0, 5, 0);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Ghost")
        {
            objekt.transform.position = new Vector3(45, 10, 9);
            GhostIsSeen = false;
            DeathTimer = 3;
        }
    }



    void Update()
    {
        if (GhostIsSeen == true)
        {
            DeathTimer -= Time.deltaTime;

            if (DeathTimer <= 0)
            {
                SceneManager.LoadScene("Dead");
            }

        }  
        



    }


}
