﻿using UnityEngine;
using System.Collections;

public class AlarmControl : MonoBehaviour {
	public GameObject player;
	public GameObject human;
	HumanController hc;
	GhostController gc;
	// Use this for initialization
	void Start () {
		hc = human.GetComponent<HumanController> ();
		gc = player.GetComponent<GhostController> ();
	}
	
	// Update is called once per frames
	void Update () {
	
	}

	void OnCollisionEnter(Collision col){
		if (col.collider.tag == "Moveable" && gc.moved == null) {
			hc.wake ();
		}
	}
}
