﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuControl : MonoBehaviour {
	void Start(){
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}

	public void NewGame(){
		SceneManager.LoadScene ("DAY-1");
	}

	public void quitGame () {
		Application.Quit ();
	}
}
